import axios from 'axios';
import api from '../libs/api';

const RETRIEVE_PRESENTATIONS = 'RETRIEVE_PRESENTATIONS';
const RETRIEVE_PRESENTATIONS_PENDING = 'RETRIEVE_PRESENTATIONS_PENDING';
const RETRIEVE_PRESENTATIONS_REJECTED = 'RETRIEVE_PRESENTATIONS_REJECTED';
const RETRIEVE_PRESENTATIONS_FULFILLED = 'RETRIEVE_PRESENTATIONS_FULFILLED';

const RETRIEVE_SINGLE_PRESENTATION = 'RETRIEVE_SINGLE_PRESENTATION';
const RETRIEVE_SINGLE_PRESENTATION_PENDING = 'RETRIEVE_SINGLE_PRESENTATION_PENDING';
const RETRIEVE_SINGLE_PRESENTATION_REJECT = 'RETRIEVE_SINGLE_PRESENTATION_REJECT';
const RETRIEVE_SINGLE_PRESENTATION_FULFILLED = 'RETRIEVE_SINGLE_PRESENTATION_FULFILLED';

const UPDATE_SLIDE_FORM = 'UPDATE_SLIDE_FORM';
const CHANGE_INDEX_SHOWN = 'CHANGE_INDEX_SHOWN';

export default {
    RETRIEVE_PRESENTATIONS_PENDING,
    RETRIEVE_PRESENTATIONS_REJECTED,
    RETRIEVE_PRESENTATIONS_FULFILLED,
    RETRIEVE_PRESENTATIONS,

    RETRIEVE_SINGLE_PRESENTATION,
    RETRIEVE_SINGLE_PRESENTATION_PENDING,
    RETRIEVE_SINGLE_PRESENTATION_REJECT,
    RETRIEVE_SINGLE_PRESENTATION_FULFILLED,

    UPDATE_SLIDE_FORM,
    CHANGE_INDEX_SHOWN,

    retrievePresentations: () => {

        return {
            type: RETRIEVE_PRESENTATIONS,
            payload: api({
                method: 'get',
                url: 'https://staging.sessionlab.com/assignment/presentations',
            }),
        };
    },

    retrieveSinglePresentation: (id) => {
        return {
            type: RETRIEVE_SINGLE_PRESENTATION,
            payload: api({
                method: 'get',
                url: `https://staging.sessionlab.com/assignment/presentations/${id}`,
            }),
        }
    },

    changeIndexShown: (index) => {
        return {
            type: CHANGE_INDEX_SHOWN,
            index,
        }
    },

    updateSlideForm: (form) => {
        return {
            type: UPDATE_SLIDE_FORM,
            form,
        };
    }
}