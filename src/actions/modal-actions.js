const OPEN_MODAL = 'OPEN_MODAL';
const CLOSE_MODAL = 'CLOSE_MODAL';


export default {
    OPEN_MODAL,
    CLOSE_MODAL,

    openModal: (content) => {
        return {
            type: OPEN_MODAL,
            content,
        };
    },

    closeModal: () => {
        return {
            type: CLOSE_MODAL,
        }
    },
}