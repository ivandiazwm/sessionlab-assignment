import axios from 'axios'

const TOKEN = process.env.SESSIONLAB_TOKEN;

export default (args) => axios({...args, headers: {'Authorization': `Bearer ${TOKEN}`}}).then(response => response.data);
