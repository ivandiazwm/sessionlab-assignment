import moment from 'moment';

function format(date) {
    return moment(date).format("D MMM YYYY hh:mm");
}

export default function parsePresentationDates(presentation) {
    return {
        ...presentation,
        created_at: format(presentation.created_at),
        updated_at: format(presentation.updated_at),
        slides: presentation.slides && presentation.slides.map(slide => ({
            ...slide,
            created_at: format(slide.created_at),
            updated_at: format(slide.updated_at),
        })),
    };
};