import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { findIndex } from 'lodash';

import PresentationActions from '../../actions/presentation-actions';
import Box from '../common/box';
import MentionsParser from '../common/mentions-parser';
import Counter from '../common/counter';

import './present-page.scss';
class PresentPage extends React.Component {

    componentDidMount() {
        if(this.props.loadingPresentation) {
            this.props.dispatch(PresentationActions.retrieveSinglePresentation(this.props.match.params.id));
        }
    }

    render() {
        const { slideIndexShown } = this.props;

        return (
            <div className="present-page">
                <div className="present-page__header">
                    <Link to={`/edit/${this.props.match.params.id}`}>
                        <button className="btn btn-default">Back to edit</button>
                    </Link>
                </div>
                <h3>Presentation</h3>
                <div className="present-page__slider">
                    <div className="present-page__navigation">
                        {slideIndexShown > 0 ? this.renderPrev() : null}
                    </div>
                    {this.renderSlide()}
                    <div className="present-page__navigation">
                        {slideIndexShown < this.getSlides().length - 1 ? this.renderNext() : null}
                    </div>
                </div>
                <Counter />
            </div>
        );
    }

    renderSlide() {
        const slide = this.getSlides()[this.props.slideIndexShown];

        return (
            <div className="present-page__slide">
                <div className="present-page__slide-text">
                    {slide.text}
                </div>
                <div className="present-page__slide-notes">
                    <MentionsParser text={slide.notes || ''} onClick={this.changeSlideId.bind(this)} />
                </div>
            </div>
        );
    }

    renderPrev() {
        const {slideIndexShown} = this.props;

        return (
            <div className="present-page__prev">
                <div className="present-page__prev-text">
                    Prev
                </div>
                <div className="present-page__prev-preview">
                    <Box
                        onClick={this.changeSlideIndex.bind(this, slideIndexShown-1)}
                        content={this.getSlides()[slideIndexShown - 1].text}/>
                </div>
            </div>
        );
    }

    renderNext() {
        const {slideIndexShown} = this.props;

        return (
            <div className="present-page__next">
                <div className="present-page__next=text">
                    Next
                </div>
                <div className="present-page__next-preview">
                    <Box
                        onClick={this.changeSlideIndex.bind(this, slideIndexShown + 1)}
                        content={this.getSlides()[slideIndexShown + 1].text}/>
                </div>
            </div>
        );
    }

    changeSlideIndex(index) {
        this.props.dispatch(PresentationActions.changeIndexShown(index));
    }

    changeSlideId(id) {
        const index = findIndex(this.props.presentation.slides, {id});

        if(index !== -1) this.changeSlideIndex(index);
    }

    getSlides() {
        return [
            ...this.props.presentation.slides,
            {text: 'END.', notes: ''}
        ];
    }
}

PresentPage.propTypes = {
    presentation: PropTypes.object,
    slideIndexShown: PropTypes.number,
};

export default connect(state => {
    return {
        presentation: state.presentation.presentation,
        slideIndexShown: state.presentation.slideIndexShown,
        loadingPresentation: state.presentation.loadingPresentation,
    };
})(PresentPage);
