import React from 'react';
import PropTypes from 'prop-types';
import { extend } from 'lodash';
import { connect } from 'react-redux';

import api from '../../libs/api';

import SlideCarousel from './slide-carousel';
import SlideForm from './slide-form';

import ModalActions from "../../actions/modal-actions";
import AreYouSure from "../common/are-you-sure";

import './presentation-editor.scss';
class PresentationEditor extends React.Component {

    render() {
        return (
            <div className="presentation-editor">
                <div className="presentation-editor__name">
                    <div>
                        <span className="presentation-editor__id"> #{this.props.presentation.id} </span>
                        <input className="presentation-editor__name-field" type="text" defaultValue={this.props.presentation.name} ref="presentationName"/>
                        <button className="btn btn-primary" onClick={this.onUpdateName.bind(this)}>Update Name</button>
                    </div>
                    <button className="btn btn-danger" onClick={this.onDeletePresentation.bind(this)}>Delete Presentation</button>
                </div>
                <div className="presentation-editor__slide-editor">
                    <SlideCarousel
                        slides={this.props.presentation.slides}
                        onSelect={this.onSlideSelected.bind(this)}
                        onPositionChange={this.onSlidePositionChanged.bind(this)}/>
                    <div className="presentation-editor__add-slide">
                        <button
                            className="btn btn-success"
                            onClick={this.onAddSlideCLick.bind(this)}>
                            Add slide
                        </button>
                    </div>
                    {(this.props.form.text) ? this.renderSlideForm() : null}
                </div>
            </div>
        );
    }

    renderSlideForm() {
        return (
            <div className="presentation-editor__slide">
                <div>Slide #{this.props.selectedIndex} </div>
                <SlideForm
                    slides={this.props.presentation.slides}
                    onSubmit={this.onSelectedSlideChanged.bind(this)}
                    value={this.props.form}
                    onChange={form => this.props.onFormChange(form)}
                    onDelete={this.onDeleteSelectedSlide.bind(this)}/>
            </div>
        );
    }

    onDeletePresentation() {
        this.props.dispatch(ModalActions.openModal(
            <AreYouSure onYes={this.deletePresentation.bind(this)}/>
        ));
    }

    onSlideSelected(index) {
        if(this.props.onSlideSelected) {
            this.props.onSlideSelected(index);
        }
    }

    onDeleteSelectedSlide(event) {
        event.preventDefault();

        this.props.dispatch(ModalActions.openModal(
            <AreYouSure onYes={this.deleteSelectedSlide.bind(this)}/>
        ));
    }

    onUpdateName() {
        const name = this.refs.presentationName.value;

        api({
            method: 'put',
            url: `https://staging.sessionlab.com/assignment/presentations/${this.props.presentation.id}`,
            data: { name }
        }).then(() => this.onChange());
    }

    onSlidePositionChanged({sourcePosition, destinationPosition}) {
        const {presentation} = this.props;
        const slideId = presentation.slides[sourcePosition].id;

        api({
            method: 'put',
            url: `https://staging.sessionlab.com/assignment/presentations/${presentation.id}/slides/${slideId}`,
            data: {
                text: presentation.slides[sourcePosition].text,
                notes: presentation.slides[sourcePosition].notes,
                position: destinationPosition + 1,
            },
        }).then(() => this.onChange());
    }

    onAddSlideCLick() {
        api({
            method: 'post',
            url: `https://staging.sessionlab.com/assignment/presentations/${this.props.presentation.id}/slides`,
            data: { text: 'new slide', position: this.props.presentation.slides.length },
        }).then(() => this.onChange());
    }

    onSelectedSlideChanged() {
        const {presentation, selectedIndex} = this.props;
        const slideId = presentation.slides[selectedIndex].id;

        api({
            method: 'put',
            url: `https://staging.sessionlab.com/assignment/presentations/${presentation.id}/slides/${slideId}`,
            data: {
                text: this.props.form.text,
                notes: this.props.form.notes
            },
        }).then(() => this.onChange());
    }

    deletePresentation() {
        api({
            method: 'delete',
            url: `https://staging.sessionlab.com/assignment/presentations/${this.props.presentation.id}`,
        }).then(() => this.props.onDeletePresentation());
    }

    deleteSelectedSlide() {
        const {presentation, selectedIndex} = this.props;
        const slideId = presentation.slides[selectedIndex].id;

        api({
            method: 'delete',
            url: `https://staging.sessionlab.com/assignment/presentations/${presentation.id}/slides/${slideId}`,
        }).then(() => this.onChange());
    }

    onChange() {
        if(this.props.onChange) {
            this.props.onChange();
        }
    }
}

PresentationEditor.propTypes = {
    presentation: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        created_at: PropTypes.string,
        updated_at: PropTypes.string,
        slides: PropTypes.arrayOf(PropTypes.object)
    }),
    onChange: PropTypes.func,
    onDeletePresentation: PropTypes.func,
    selectedIndex: PropTypes.number,
    onSlideSelected: PropTypes.func
};

export default connect()(PresentationEditor);
