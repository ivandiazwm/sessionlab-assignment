import React from 'react';
import PropTypes from 'prop-types';
import { extend } from 'lodash';
import classNames from 'classnames';

import { MentionsInput, Mention } from 'react-mentions'

import './slide-form.scss';
class SlideForm extends React.Component {

    render() {
        return (
            <div className="slide-form">
                <form onSubmit={this.onFormSubmit.bind(this)}>
                    <div className="slide-form__field">
                        <label>
                            Text:
                            <input
                                size={30}
                                name="text"
                                value={this.props.value.text || ''}
                                onChange={this.onFormChange.bind(this, 'text')} />
                        </label>
                    </div>
                    <div className="slide-form__field">
                        <label>
                            Notes:
                            <MentionsInput
                                className="slide-form__notes-field"
                                name="notes"
                                value={this.props.value.notes || ''}
                                onChange={this.onFormChange.bind(this, 'notes')}>
                                <Mention
                                    trigger="@"
                                    data={this.getSlideMentionOptions()}
                                    renderSuggestion={this.renderMentionSuggestion.bind(this)}
                                    appendSpaceOnAdd
                                />
                            </MentionsInput>
                        </label>
                    </div>
                    <button className="slide-form__button btn btn-primary">Save</button>
                    <button className="slide-form__button btn btn-danger" onClick={this.props.onDelete}>Delete</button>
                </form>
            </div>
        );
    }

    renderMentionSuggestion(entry, search, highlightedDisplay, index, focused)	 {
        const classes = {
            'slide-form__mention-suggestion': true,
            'slide-form__mention-suggestion--focused': focused,
        };

        return (
            <div className={classNames(classes)}>
                <div className="slide-form__mention-suggestion-id">
                    {highlightedDisplay}
                </div>
                <div className="slide-form__mention-suggestion-description">
                    {entry.text}
                </div>
            </div>
        )
    }

    getSlideMentionOptions() {
        return this.props.slides.map(slide => {
            return {
                id: slide.id,
                display: `@${slide.id}`,
                text: slide.text,
            };
        });
    }

    onFormSubmit(event) {
        event.preventDefault();

        if(this.props.onSubmit) {
            this.props.onSubmit(this.props.value);
        }
    }

    onFormChange(field, event) {
        if(this.props.onChange) {
            this.props.onChange(extend({}, this.props.value, {[field]: event.target.value}));
        }
    }
}

SlideForm.propTypes = {
    slides: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
    })),
    value: PropTypes.shape({
        text: PropTypes.string,
        notes: PropTypes.string,
    }),
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    onDelete: PropTypes.func,
};

export default SlideForm;