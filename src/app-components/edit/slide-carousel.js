import React from 'react';
import PropTypes from 'prop-types';

import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import Box from '../common/box';

import './slide-carousel.scss';
class SlideCarousel extends React.Component {

    render() {
        return (
            <div className="slide-carousel">
                <DragDropContext
                    onDragEnd={this.onDragEnd.bind(this)}>
                    <Droppable droppableId="slide-items" direction="horizontal">
                        {(provided) => (
                            <div className="slide-carousel__items"
                                 ref={provided.innerRef}
                                 {...provided.draggableProps}
                                 {...provided.dragHandleProps} >
                                {this.props.slides.map(this.renderSlide.bind(this))}
                            </div>
                        )}
                    </Droppable>
                </DragDropContext>
            </div>
        );
    }

    renderSlide(slide, index) {
        return (
            <Draggable key={slide.id} draggableId={slide.id} index={index}>
                {(provided) => (
                    <div className="slide-carousel__item"
                         {...provided.draggableProps}
                         {...provided.dragHandleProps}
                         ref={provided.innerRef}>
                        <span className="slide-carousel__item-id">
                            @slide{slide.id}
                        </span>
                        <Box
                            key={index}
                            content={slide.text}
                            type="small"
                            updatedDate={slide.updated_at}
                            onClick={this.onSlideClick.bind(this, index)} />
                    </div>
                )}
            </Draggable>
        );
    }

    onSlideClick(index) {
        if(this.props.onSelect) {
            this.props.onSelect(index);
        }
    }

    onDragEnd(result) {
        if(this.props.onPositionChange && result.source.index !== result.destination.index) {
            this.props.onPositionChange({
                sourcePosition: result.source.index,
                destinationPosition: result.destination.index,
            });
        }
    }

}

SlideCarousel.propTypes = {
    slides: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
        updated_at: PropTypes.string,
    })),
    onSelect: PropTypes.func,
    onPositionChange: PropTypes.func,
};

export default SlideCarousel;