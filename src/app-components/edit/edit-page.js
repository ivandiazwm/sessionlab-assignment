import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import PresentationActions from '../../actions/presentation-actions';
import PresentationEditor from './presentation-editor';

import './edit-page.scss';
class EditPage extends React.Component {

    componentDidMount() {
        this.retrievePresentation()
    }

    render() {
        return (
            <div className="edit-page">
                <div className="edit-page__header">
                    <Link to="/">
                        <button className="btn btn-default">Back to home</button>
                    </Link>
                    <Link to={`/present/${this.props.match.params.id}`}>
                        <button className="btn btn-primary">Run Presentation</button>
                    </Link>
                </div>
                <h3>Edit Presentation</h3>
                <div className="edit-page__presentation-editor-container">
                    <PresentationEditor
                        form={this.props.slideForm}
                        presentation={this.props.presentation}
                        onChange={this.retrievePresentation.bind(this)}
                        onFormChange={this.onFormChange.bind(this)}
                        onDeletePresentation={this.onPresentationDeleted.bind(this)}
                        selectedIndex={this.props.slideIndexShown}
                        onSlideSelected={this.changeSlideIndex.bind(this)}
                    />
                </div>
            </div>
        );
    }

    retrievePresentation() {
        this.props.dispatch(PresentationActions.retrieveSinglePresentation(this.props.match.params.id));
    }

    changeSlideIndex(index) {
        this.props.dispatch(PresentationActions.changeIndexShown(index));
    }

    onFormChange(form) {
        this.props.dispatch(PresentationActions.updateSlideForm(form));
    }

    onPresentationDeleted() {
        this.props.history.push('/');
    }
}


export default connect(state => {
    return {
        loading: state.presentation.loading,
        presentation: state.presentation.presentation,
        slideIndexShown: state.presentation.slideIndexShown,
        slideForm: state.presentation.slideForm,
    };
})(EditPage);
