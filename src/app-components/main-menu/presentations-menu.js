import React from 'react';
import PropTypes from 'prop-types';
import api from '../../libs/api';

import Box from '../common/box';

import './presentations-menu.scss';
class PresentationsMenu extends React.Component {

    render() {
        return (
            <div className="presentations-menu">
                {this.props.presentations.map(this.renderPresentationBox.bind(this))}
                <Box className="presentations-menu__add-new-item" content="Add new +" onClick={this.addPresentation.bind(this)}/>
            </div>
        );
    }

    renderPresentationBox(presentation, index) {
        return (
            <Box
                key={index}
                className="presentations-menu__item"
                content={presentation.name}
                updatedDate={presentation.updated_at}
                link={`/edit/${presentation.id}`}/>
        );
    }

    addPresentation() {
        api({
            method: 'post',
            url: 'https://staging.sessionlab.com/assignment/presentations',
            data: {
                name: 'New Presentation'
            }
        }).then(() => this.props.onAddPresentation());
    }
}

PresentationsMenu.propTypes = {
    presentations: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        updated_at: PropTypes.string,
    })),
    onAddPresentation: PropTypes.func
};

export default PresentationsMenu;