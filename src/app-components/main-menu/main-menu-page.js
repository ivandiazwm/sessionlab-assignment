import React from 'react';
import PresentationsMenu from './presentations-menu';
import connect from "react-redux/es/connect/connect";

import PresentationActions from "../../actions/presentation-actions";

class MainMenuPage extends React.Component {
    componentDidMount() {
        this.retrievePresentations();
    }

    render() {
        return (
            <div>
                Select the presentation you want to work on
                <PresentationsMenu presentations={this.props.presentations} onAddPresentation={this.retrievePresentations.bind(this)}/>
            </div>
        );
    }

    retrievePresentations() {
        this.props.dispatch(PresentationActions.retrievePresentations());
    }
}


export default connect(state => {
    return {
        presentations: state.presentation.presentations
    };
})(MainMenuPage);