import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ModalActions from '../../actions/modal-actions';

import './are-you-sure.scss';
class AreYouSure extends React.Component {
    render() {
        return (
            <div className="are-you-sure">
                <h4 className="are-you-sure__text">Are you sure?</h4>
                <button className="are-you-sure__button btn btn-danger" onClick={this.onYes.bind(this)}>Yes</button>
                <button className="are-you-sure__button btn btn-default" onClick={this.closeModal.bind(this)}>No</button>
            </div>
        );
    }

    onYes() {
        if(this.props.onYes) {
            this.props.onYes();
        }
        this.closeModal();
    }

    closeModal() {
        this.props.dispatch(ModalActions.closeModal());
    }
}

AreYouSure.propTypes = {
    onYes: PropTypes.func,
};

export default connect()(AreYouSure);