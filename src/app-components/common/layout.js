import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ModalActions from '../../actions/modal-actions';

import Modal from './modal';

import './layout.scss'
class Layout extends React.Component {

    render() {
        return (
            <div className="layout">
                {this.props.children}
                {this.props.modalOpened ? <Modal content={this.props.modalContent} onClose={this.closeModal.bind(this)} /> : null}
                {this.props.loading ? <Modal content={'loading...'} /> : null}
            </div>
        );
    }

    closeModal() {
        this.props.dispatch(ModalActions.closeModal());
    }
}

Layout.propTypes = {
    modalOpened: PropTypes.bool,
    modalContent: PropTypes.node,
};

export default connect((state) => {
    return {
        loading: state.presentation.loading,
        modalOpened: state.modal.opened,
        modalContent: state.modal.content,
    };
})(Layout);