// Based on https://github.com/opensupports/opensupports/blob/master/client/src/lib-app/mentions-parser.js
import React from 'react';
import PropTypes from 'prop-types';

const PARSING_TEXT = 0;
const PARSING_MENTION = 1;

class MentionsParser extends React.Component {

    render() {
        return (
            <div>
                {this.parse(this.props.text)}
            </div>
        )
    }

    parse(text) {
        let parsingLink = false;
        let parsingType = PARSING_TEXT;
        let parsingSegment = '';
        let ans = [];

        text = text.replace(/@\[([^\[\]]+)\]\(([^)]+)\)/, '$1');

        for(let index = 0; index < text.length; ++index){
            let character = text[index];

            if(character === '@') {
                ans.push(this.compileSegment(parsingSegment, parsingType, ans.length));

                parsingLink = true;
                parsingType = PARSING_MENTION;
                parsingSegment = '';
            } else if(!this.isDigit(character) && parsingLink) {
                ans.push(this.compileSegment(parsingSegment, parsingType, ans.length));

                parsingLink = false;
                parsingType = PARSING_TEXT;
                parsingSegment = character;
            } else {
                parsingSegment += character;
            }
        }

        ans.push(this.compileSegment(parsingSegment, parsingType, ans.length));

        return ans;
    }

    isDigit(string){
        return /[0-9]/.test(string);
    }

    compileSegment(segment, parsingType, index){
        switch(parsingType){
            case PARSING_TEXT:
                return (
                    <span key={index}>
                        {segment}
                    </span>
                );
            case PARSING_MENTION:
                if(segment.length)
                    return (
                        <span key={index} onClick={() => this.props.onClick(parseInt(segment))} style={{color: 'blue', cursor: 'pointer'}}>
                            @{segment}
                        </span>
                    );
                else
                    return <span key={index}>@</span>;
            default:
                return <span key={index} />;
        }
    }
}

MentionsParser.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
};

export default MentionsParser;