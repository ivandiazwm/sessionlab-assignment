import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import classNames from 'classnames';

import './box.scss';
class Box extends React.Component {

    render() {
        return (
            <div className={this.getClass()}>
                {this.props.link ? this.renderContentWithLink() : this.renderContent()}
                {this.props.updatedDate && this.renderUpdatedDate()}
            </div>
        );
    }

    renderContentWithLink() {
        return (
            <Link to={this.props.link} className="box__link">
                {this.renderContent()}
            </Link>
        );
    }

    renderContent() {
        return (
            <div className="box__content" onClick={this.props.onClick}>
                <span className="box__content-text">{this.props.content}</span>
            </div>
        );
    }


    renderUpdatedDate() {
        return (
            <div className="box__date">
                Last updated on {this.props.updatedDate}
            </div>
        );
    }

    getClass() {
        const classes = {
            'box': true,
            'box--small': this.props.type === 'small',
            [this.props.className]: (this.props.className)
        };

        return classNames(classes);
    }
}

Box.propTypes = {
    type: PropTypes.oneOf(['small', 'medium']),
    content: PropTypes.node,
    updatedDate: PropTypes.string,
    onClick: PropTypes.func,
};

export default Box;