import React from 'react';
import moment from 'moment';

class Counter extends React.Component {

    constructor(props) {
        super(props);
        this.initialTime = moment();
    }

    componentDidMount() {
        this.interval = setInterval(() => this.forceUpdate(), 1000);
    }

    componentWIllUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div>
                Counter: {this.getElapsedTime()}
            </div>
        );
    }

    getElapsedTime() {
        const currentTime = moment();
        const duration = moment.duration(currentTime.diff(this.initialTime));

        return `${duration.minutes()}min ${duration.seconds()}s`;
    }
}

export default Counter;