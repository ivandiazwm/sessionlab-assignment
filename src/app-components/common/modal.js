import React from 'react';
import PropTypes from 'prop-types';

import './modal.scss';
class Modal extends React.Component {

    render() {
        return (
            <div className="custom-modal">
                <div className="custom-modal__content">
                    {this.props.content}
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    content: PropTypes.node,
    onClose: PropTypes.func,
};

export default Modal;