import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Layout from './app-components/common/layout';
import MainMenuPage from './app-components/main-menu/main-menu-page';
import EditPage from './app-components/edit/edit-page';
import PresentPage from './app-components/present/present-page';

export default (
    <Router>
        <Layout>
            <Switch>
                <Route exact path="/" component={MainMenuPage} />
                <Route path="/edit/:id" component={EditPage} />
                <Route path="/present/:id" component={PresentPage} />
            </Switch>
        </Layout>
    </Router>
);