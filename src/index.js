import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import promise from 'redux-promise-middleware';

import reducers from './reducers';

const composeStoreWithMiddleware = applyMiddleware(promise)(createStore);
const store = composeStoreWithMiddleware(reducers);

import Routes from './routes';

ReactDOM.render(
    <Provider store={store}>
        {Routes}
    </Provider>,
    document.getElementById('app'),
);

module.hot.accept();
