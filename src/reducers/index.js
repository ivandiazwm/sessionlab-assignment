import { combineReducers } from 'redux';
import modalReducer from './modal-reducer';
import presentationReducer from './presentation-reducer';

const reducers = combineReducers({
    modal: modalReducer,
    presentation: presentationReducer,
});

export default reducers;