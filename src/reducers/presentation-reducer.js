import { sortBy } from 'lodash';

import parsePresentationDates from '../libs/parse-presentation-dates';
import PresentationActions from '../actions/presentation-actions';

const initialState = {
    loading: true,
    loadingPresentation: true,
    presentations: [],
    presentation: {
        slides: []
    },
    slideForm: {},
    slideIndexShown: 0,
};

export default function presentationReducer(state = initialState, action) {
    switch (action.type) {
        case PresentationActions.RETRIEVE_PRESENTATIONS_PENDING:
            return {
                ...state,
                loading: true,
            };

        case PresentationActions.RETRIEVE_PRESENTATIONS_FULFILLED:
            return {
                ...state,
                loading: false,
                presentations: action.payload && action.payload.map(parsePresentationDates),
            };

        case PresentationActions.RETRIEVE_SINGLE_PRESENTATION_PENDING:
            return {
                ...state,
                loading: true,
                loadingPresentation: true,
            };

        case PresentationActions.RETRIEVE_SINGLE_PRESENTATION_FULFILLED:
            const slideIndexShown = Math.min(state.slideIndexShown, action.payload.slides.length - 1);

            return {
                ...state,
                loading: false,
                loadingPresentation: false,
                presentation: parsePresentationDates({
                    ...action.payload,
                    slides: sortBy(action.payload.slides, 'position'),
                }),
                slideIndexShown: Math.max(0, slideIndexShown),
                slideForm: (slideIndexShown !== -1) ? action.payload.slides[slideIndexShown] : {},
            };

        case PresentationActions.CHANGE_INDEX_SHOWN:
            return {
                ...state,
                slideIndexShown: action.index,
                slideForm: state.presentation.slides[action.index],
            };

        case PresentationActions.UPDATE_SLIDE_FORM:
            return {
                ...state,
                slideForm: action.form
            };

        default:
            return state;
    }
};