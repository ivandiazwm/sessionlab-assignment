import ModalActions from '../actions/modal-actions';
import { extend } from 'lodash';

const initialState = {
    opened: false,
    loading: false,
    content: null,
};

export default function modalReducer(state = initialState, action) {
    switch (action.type) {
        case ModalActions.OPEN_MODAL:
            return extend({}, state, {
                opened: true,
                content: action.content
            });

        case ModalActions.CLOSE_MODAL:
            return extend({}, state, {
                opened: false,
                content: null
            });

        default:
            return state;
    }
};