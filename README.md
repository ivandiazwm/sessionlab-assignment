## Simple Slides Assignment
This is an assignment for an interview at SessionLab.

### How to run
First, install dependencies
```
npm install
```

Remember to set the api token as an environment variable
```
export SESSIONLAB_TOKEN='token'
```

Now you have the token defined, you can run it by
```
npm start
```

After this, server should be running at `http://localhost:8080/`.
